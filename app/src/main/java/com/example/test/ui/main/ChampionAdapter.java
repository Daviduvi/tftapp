package com.example.test.ui.main;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.test.R;

import java.util.List;

public class ChampionAdapter extends ArrayAdapter<Champion> {
    public ChampionAdapter(Context context, int lv_champions_row, int resource, List<Champion> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Champion champion = getItem(position);
        Log.w("XXXX", champion.toString());


        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lv_champions_row, parent, false);
        }

        TextView tvName = convertView.findViewById(R.id.tvName);
        TextView tvTraitPri = convertView.findViewById(R.id.tvTraitPri);
        TextView tvTraitSec = convertView.findViewById(R.id.tvTraitSec);
        TextView tvTraitTri = convertView.findViewById(R.id.tvTraitTri);
        ImageView ivChampion = convertView.findViewById(R.id.ivChampion);

        tvName.setText(champion.getName());
        tvTraitPri.setText(champion.getTrait_pri());
        tvTraitSec.setText(champion.getTrait_sec());
        if (!champion.getTrait_tri().isEmpty()) {
            tvTraitTri.setText(champion.getTrait_tri());
        } else {
            tvTraitTri.setText("");
        }
        Glide.with(getContext()).load(champion.getProfileImageUrl()).into(ivChampion);

        return convertView;
    }
}
