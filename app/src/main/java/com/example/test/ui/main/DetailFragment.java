package com.example.test.ui.main;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.test.R;

public class DetailFragment extends Fragment {

    private MainViewModel2 mViewModel;
    private  View view;
    private TextView tvName;
    private TextView tvTraitPri;
    private TextView tvTraitSec;
    private TextView tvTraitTri;
    private ImageView ivImageUrl;

    public static DetailFragment newInstance() {
        return new DetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_fragment2, container, false);

        Intent i = getActivity().getIntent();

        if (i != null) {
            Champion champion = (Champion) i.getSerializableExtra("champion");

            if (champion != null) {
                updateUi(champion);
            }
        }

        return view;
    }

    private void updateUi(Champion champion) {
        Log.d("Champion", champion.toString());

        ivImageUrl = view.findViewById(R.id.ivImageUrl);

        Glide.with(getContext()).load(champion.getImageUrl()).into(ivImageUrl);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel2.class);
        // TODO: Use the ViewModel
    }

}