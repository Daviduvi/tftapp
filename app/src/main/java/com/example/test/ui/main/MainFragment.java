package com.example.test.ui.main;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.test.R;

import java.util.ArrayList;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private ArrayList<Champion> items;
    private ChampionAdapter adapter;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);

        ListView lvChampions = view.findViewById(R.id.lvChampions);

        items = new ArrayList<>();

        adapter = new ChampionAdapter(
                getContext(),
                R.layout.lv_champions_row,
                R.id.tvName,
                items
        );

        lvChampions.setAdapter(adapter);

        lvChampions.setOnItemClickListener((adapter, fragment, i, l) -> {
            Champion champion = (Champion) adapter.getItemAtPosition(i);
            Intent intent = new Intent(getContext(), DetailActivity.class);
            intent.putExtra("champion", champion);

            startActivity(intent);
        });

        return view;
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Champion>> {
        @Override
        protected ArrayList<Champion> doInBackground(Void... voids) {
            TFTApi api = new TFTApi();
            ArrayList<Champion> result = api.getChampions();

            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Champion> champions) {
            adapter.clear();
            for (Champion champion : champions) {
                adapter.add(champion);
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_refresh, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_refresh){
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void refresh(){
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

}