package com.example.test.ui.main;

import java.io.Serializable;

public class Champion implements Serializable {

    private int id;
    private String name;
    private int cost;
    private String trait_pri;
    private String trait_sec;
    private String trait_tri;
    private String imageUrl;
    private String profileImageUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getTrait_pri() {
        return trait_pri;
    }

    public void setTrait_pri(String trait_pri) {
        this.trait_pri = trait_pri;
    }

    public String getTrait_sec() {
        return trait_sec;
    }

    public void setTrait_sec(String trait_sec) {
        this.trait_sec = trait_sec;
    }

    public String getTrait_tri() {
        return trait_tri;
    }

    public void setTrait_tri(String trait_tri) {
        this.trait_tri = trait_tri;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    @Override
    public String toString() {
        return "Champion{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cost=" + cost +
                ", trait_pri='" + trait_pri + '\'' +
                ", trait_sec='" + trait_sec + '\'' +
                ", trait_tri='" + trait_tri + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", profileImageUrl='" + profileImageUrl + '\'' +
                '}';
    }
}
